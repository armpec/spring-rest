DROP TABLE IF EXISTS car;
DROP TABLE IF EXISTS user_role;
DROP TABLE IF EXISTS role;
DROP TABLE IF EXISTS "user";
DROP TABLE IF EXISTS company;
DROP TABLE IF EXISTS file;

CREATE TABLE company
(
    id      BIGSERIAL PRIMARY KEY NOT NULL,
    name    VARCHAR(255) UNIQUE   NOT NULL,
    created TIMESTAMP             NOT NULL DEFAULT NOW(),
    updated TIMESTAMP             NOT NULL DEFAULT NOW()
);

CREATE TABLE "user"
(
    id         BIGSERIAL PRIMARY KEY NOT NULL,
    username   VARCHAR(255) UNIQUE   NOT NULL,
    password   VARCHAR(255)          NOT NULL,
    first_name VARCHAR(255)          NOT NULL,
    last_name  VARCHAR(255)          NOT NULL,
    age        INTEGER               NOT NULL,
    company_id BIGINT                NOT NULL REFERENCES company (id),
    created    TIMESTAMP             NOT NULL DEFAULT NOW(),
    updated    TIMESTAMP             NOT NULL DEFAULT NOW()
);

CREATE TABLE role
(
    id   BIGSERIAL PRIMARY KEY NOT NULL,
    name VARCHAR(255) UNIQUE   NOT NULL
);

CREATE TABLE user_role
(
    user_id BIGINT NOT NULL REFERENCES "user" (id),
    role_id BIGINT NOT NULL REFERENCES role (id)
);

CREATE TABLE car
(
    id           BIGSERIAL PRIMARY KEY NOT NULL,
    manufacturer VARCHAR(255)          NOT NULL,
    model        VARCHAR(255)          NOT NULL,
    year         INTEGER               NOT NULL,
    color        VARCHAR(255)          NOT NULL,
    description  TEXT                  NOT NULL,
    value        NUMERIC(10, 2)        NOT NULL,
    user_id      BIGINT REFERENCES "user" (id),
    created      TIMESTAMP             NOT NULL DEFAULT NOW(),
    updated      TIMESTAMP             NOT NULL DEFAULT NOW()
);

CREATE TABLE file
(
    id       BIGSERIAL PRIMARY KEY NOT NULL,
    filename VARCHAR(255)          NOT NULL,
    size     BIGINT                NOT NULL,
    bytes    BYTEA                 NOT NULL,
    created  TIMESTAMP             NOT NULL DEFAULT NOW(),
    updated  TIMESTAMP             NOT NULL DEFAULT NOW()
);

INSERT INTO company (name)
VALUES ('Microsoft'),
       ('Apple');

INSERT INTO "user" (username, password, first_name, last_name, age, company_id)
VALUES ('admin', '{bcrypt}$2a$10$WJvAKW5R1VM2SSaAWf0WYO/FBcovz6X3BpulRoS2FWdUbcCZPo8V2', 'John', 'Admin', 48, 1),
       ('user', '{bcrypt}$2a$10$WJvAKW5R1VM2SSaAWf0WYO/FBcovz6X3BpulRoS2FWdUbcCZPo8V2', 'Michael', 'User', 24, 2);

INSERT INTO role (name)
VALUES ('USER'),
       ('ADMIN');

INSERT INTO user_role (user_id, role_id)
VALUES (1, 1),
       (1, 2),
       (2, 1);

INSERT INTO car (manufacturer, model, year, color, description, value, user_id)
VALUES ('Toyota', 'Prius', 2010, 'black', 'Sėdi ir važiuoji', 6999.99, 1),
       ('Audi', 'Q7', 2019, 'green', 'Variklio defektas', 2999.99, 2);
