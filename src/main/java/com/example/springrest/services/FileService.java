package com.example.springrest.services;

import com.example.springrest.entities.File;
import com.example.springrest.exceptions.FileException;
import com.example.springrest.exceptions.FileNotFoundException;
import com.example.springrest.repositories.FileRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Set;

@Service
public class FileService {

    @Value("${uploads.dir}")
    private String dir;

    @Value("${uploads.filesize.max-bytes}")
    private Long maxSize;

    private final FileRepository fileRepository;

    public FileService(FileRepository fileRepository) {
        this.fileRepository = fileRepository;
    }

    public void uploadImage(MultipartFile image) {
        validateImage(image);

        String filename = String.format("%s_%d.%s", image.getOriginalFilename().split("\\.")[0],
                System.currentTimeMillis(), image.getOriginalFilename().split("\\.")[1]);

        try {
            image.transferTo(Path.of(dir + "/" + filename));
        } catch (IOException e) {
            throw new FileException("Cannot create file");
        }
    }

    public byte[] getImage(String filename) {
        try {
            return Files.readAllBytes(Path.of(dir + "/" + filename));
        } catch (IOException e) {
            throw new FileNotFoundException(filename);
        }
    }

    public void uploadBlobImage(MultipartFile image) {
        validateImage(image);

        String filename = String.format("%s_%d.%s", image.getOriginalFilename().split("\\.")[0],
                System.currentTimeMillis(), image.getOriginalFilename().split("\\.")[1]);

        File file = new File();
        file.setFilename(filename);
        file.setSize(image.getSize());

        try {
            file.setBytes(image.getBytes());
        } catch (IOException e) {
            throw new FileException("Cannot create file");
        }

        fileRepository.save(file);
    }

    public byte[] getBlobImage(String filename) {
        return fileRepository.findByFilename(filename).orElseThrow(() -> new FileNotFoundException(filename)).getBytes();
    }


    private void validateImage(MultipartFile image) {
        Set<String> allowedTypes = Set.of(MediaType.IMAGE_GIF_VALUE, MediaType.IMAGE_JPEG_VALUE, MediaType.IMAGE_PNG_VALUE);

        if (!allowedTypes.contains(image.getContentType())) {
            throw new FileException("File media type is not allowed");
        }

        if (image.getSize() > maxSize) {
            throw new FileException("Image size cannot be more than " + maxSize);
        }
    }
}
