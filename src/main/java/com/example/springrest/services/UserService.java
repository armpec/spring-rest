package com.example.springrest.services;

import com.example.springrest.entities.User;
import com.example.springrest.exceptions.ResourceNotFoundException;
import com.example.springrest.exceptions.UserAlreadyExistsException;
import com.example.springrest.exceptions.UserNotFoundException;
import com.example.springrest.repositories.RoleRepository;
import com.example.springrest.repositories.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class UserService implements UserDetailsService {

    private final CompanyService companyService;

    private final UserRepository userRepository;

    private final RoleRepository roleRepository;

    private final PasswordEncoder encoder;

    public UserService(CompanyService companyService, UserRepository userRepository, RoleRepository roleRepository, PasswordEncoder encoder) {
        this.companyService = companyService;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.encoder = encoder;
    }

    public List<User> getUsers() {
        return userRepository.findAll();
    }

    public User getUser(Long id) {
        return userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(id));
    }

    public User createUser(User user, Long companyId, boolean isAdmin) {
        if (userRepository.findUserByUsername(user.getUsername()).isPresent()) {
            throw new UserAlreadyExistsException(user.getUsername());
        }

        if (isAdmin) {
            // for demo purposes only, user should not be able to register as admin by himself
            user.setRoles(Set.of(roleRepository.findRoleByName("ADMIN").get(),
                    roleRepository.findRoleByName("USER").get()));
        } else {
            user.setRoles(Set.of(roleRepository.findRoleByName("USER").get()));
        }

        user.setPassword(encoder.encode(user.getPassword()));
        user.setCompany(companyService.getCompany(companyId));

        return userRepository.save(user);
    }

    public void deleteUser(Long id) {
        userRepository.deleteById(getUser(id).getId());
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findUserByUsername(username).orElseThrow(() -> new UserNotFoundException(username));
    }
}
