package com.example.springrest.services;

import com.example.springrest.entities.Company;
import com.example.springrest.exceptions.ResourceNotFoundException;
import com.example.springrest.repositories.CompanyRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyService {

    private final CompanyRepository companyRepository;

    public CompanyService(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    public List<Company> getCompanies() {
        return companyRepository.findAll();
    }

    public Company getCompany(Long id) {
        return companyRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(id));
    }
}
