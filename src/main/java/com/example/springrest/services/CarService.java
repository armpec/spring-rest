package com.example.springrest.services;

import com.example.springrest.entities.Car;
import com.example.springrest.entities.User;
import com.example.springrest.exceptions.ResourceNotFoundException;
import com.example.springrest.repositories.CarRepository;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarService {

    private final UserService userService;

    private final CarRepository carRepository;

    public CarService(UserService userService, CarRepository carRepository) {
        this.userService = userService;
        this.carRepository = carRepository;
    }

    public List<Car> getCars(String color, String search, Long companyId) {
        if (companyId != null) {
            return carRepository.findAllByOwnerCompanyId(companyId);
        } else if (color != null & search != null) {
            return carRepository.searchCars(search, color);
        } else if (color != null) {
            return carRepository.findAllByColor(color);
        } else if (search != null) {
            return carRepository.searchCars(search);
        } else {
            return carRepository.findAll();
        }
    }

    public Car getCar(Long id) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Car car = carRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(id));

        if (user.isAdmin() || (car.getOwner() != null && car.getOwner().getId().equals(user.getId()))) {
            return car;
        }

        throw new AccessDeniedException("Access denied: user.id = " + user.getId() + ", car.id = " + car.getId());
    }

    public Car createCar(Car car, Long ownerId) {
        car.setOwner(ownerId != null ? userService.getUser(ownerId) : null);
        return carRepository.save(car);
    }

    public Car updateCar(Car updatedCar, Long ownerId) {
        Car car = getCar(updatedCar.getId());

        updatedCar.setOwner(ownerId != null ? userService.getUser(ownerId) : null);
        updatedCar.setCreated(car.getCreated());

        return carRepository.save(updatedCar);
    }

    public void setOwner(Long id, Long ownerId) {
        Car car = getCar(id);

        car.setOwner(ownerId != null ? userService.getUser(ownerId) : null);

        carRepository.save(car);
    }

    public void deleteCar(Long id) {
        carRepository.deleteById(getCar(id).getId());
    }
}
