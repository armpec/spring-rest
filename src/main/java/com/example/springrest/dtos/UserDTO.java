package com.example.springrest.dtos;

import com.example.springrest.entities.Role;
import com.example.springrest.entities.User;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@NoArgsConstructor
public class UserDTO {

    private Long id;

    private Long companyId;

    private String username;

    private String firstName;

    private String lastName;

    private Integer age;

    private Set<Role> roles;

    private Boolean isAdmin;

    private String created;

    private String updated;

    public UserDTO(User user) {
        this.id = user.getId();
        this.companyId = user.getCompany().getId();
        this.username = user.getUsername();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.age = user.getAge();
        this.roles = user.getRoles();
        this.isAdmin = user.isAdmin();
        this.created = user.getCreated().toString();
        this.updated = user.getUpdated().toString();
    }
}
