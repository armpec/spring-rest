package com.example.springrest.dtos.responses;

import com.example.springrest.entities.Car;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class CreateCarResponse {

    private Long id;

    private Long ownerId;

    private String manufacturer;

    private String model;

    private Integer year;

    private String color;

    private String description;

    private BigDecimal value;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime created;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updated;

    public CreateCarResponse(Car car) {
        this.id = car.getId();
        this.ownerId = car.getOwner() != null ? car.getOwner().getId() : null;
        this.manufacturer = car.getManufacturer();
        this.model = car.getModel();
        this.year = car.getYear();
        this.color = car.getColor();
        this.description = car.getDescription();
        this.value = car.getValue();
        this.created = car.getCreated();
        this.updated = car.getUpdated();
    }
}
