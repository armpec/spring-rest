package com.example.springrest.dtos;

import com.example.springrest.entities.Company;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CompanyDTO {

    private Long id;

    private String name;

    private String created;

    private String updated;

    public CompanyDTO(Company company) {
        this.id = company.getId();
        this.name = company.getName();
        this.created = company.getCreated().toString();
        this.updated = company.getUpdated().toString();
    }
}
