package com.example.springrest.dtos.requests;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class AssignCarRequest {

    @NotNull
    private Long carId;
}
