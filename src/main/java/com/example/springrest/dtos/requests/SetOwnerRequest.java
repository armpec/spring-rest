package com.example.springrest.dtos.requests;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SetOwnerRequest {

    private Long ownerId;
}
