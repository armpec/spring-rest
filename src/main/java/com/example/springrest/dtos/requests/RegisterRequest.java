package com.example.springrest.dtos.requests;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
public class RegisterRequest {

    private Long companyId;

    @NotBlank
    private String username;

    @Size(min = 6, max = 30)
    private String password;

    @NotBlank
    private String firstName;

    @NotBlank
    private String lastName;

    @NotNull
    @Range(min = 1, max = 140)
    private Integer age;

    // for demo purposes only, user should not be able to register as admin by himself
    private Boolean isAdmin = false;
}
