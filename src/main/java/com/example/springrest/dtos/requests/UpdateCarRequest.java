package com.example.springrest.dtos.requests;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class UpdateCarRequest {

    private Long ownerId;

    @NotBlank
    private String manufacturer;

    @NotBlank
    private String model;

    @NotNull
    @Range(min = 1900, max = 2100)
    private Integer year;

    @NotBlank
    private String color;

    @NotBlank
    private String description;

    @Min(0)
    @NotNull
    private BigDecimal value;
}
