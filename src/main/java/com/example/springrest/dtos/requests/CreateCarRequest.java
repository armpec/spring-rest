package com.example.springrest.dtos.requests;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateCarRequest {

    private Long ownerId;

    @NotBlank
    @ApiModelProperty(notes = "Car manufacturer", required = true, name = "manufacturer", value = "Toyota", example = "Toyota")
    private String manufacturer;

    @NotBlank
    @ApiModelProperty(notes = "Car model", required = true, name = "model", value = "RAV4", example = "RAV4")
    private String model;

    @NotNull
    @Range(min = 1900, max = 2100)
    @ApiModelProperty(notes = "Car year", required = true, name = "year", value = "2010", example = "2010")
    private Integer year;

    @NotBlank
    @ApiModelProperty(notes = "Car color", required = true, name = "color", value = "black", example = "black")
    private String color;

    @NotBlank
    @ApiModelProperty(notes = "Car description", required = true, name = "description", value = "...", example = "...")
    private String description;

    @NotNull
    @Positive
    @ApiModelProperty(notes = "Car value", required = true, name = "value", value = "9999.99", example = "9999.99")
    private BigDecimal value;
}
