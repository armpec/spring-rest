package com.example.springrest.entities;

import com.example.springrest.dtos.requests.CreateCarRequest;
import com.example.springrest.dtos.requests.UpdateCarRequest;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "car")
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "manufacturer")
    private String manufacturer;

    @Column(name = "model")
    private String model;

    @Column(name = "year")
    private Integer year;

    @Column(name = "color")
    private String color;

    @Column(name = "description")
    private String description;

    @Column(name = "value")
    private BigDecimal value;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User owner;

    @CreationTimestamp
    @Column(name = "created")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime created;

    @UpdateTimestamp
    @Column(name = "updated")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updated;

    public Car(String manufacturer, String model, Integer year, String color, String description, BigDecimal value, User owner) {
        this.manufacturer = manufacturer;
        this.model = model;
        this.year = year;
        this.color = color;
        this.description = description;
        this.value = value;
        this.owner = owner;
    }

    public Car(CreateCarRequest createCarRequest) {
        this.manufacturer = createCarRequest.getManufacturer();
        this.model = createCarRequest.getModel();
        this.year = createCarRequest.getYear();
        this.color = createCarRequest.getColor();
        this.description = createCarRequest.getDescription();
        this.value = createCarRequest.getValue();
    }

    public Car(Long id, UpdateCarRequest updateCarRequest) {
        this.id = id;
        this.manufacturer = updateCarRequest.getManufacturer();
        this.model = updateCarRequest.getModel();
        this.year = updateCarRequest.getYear();
        this.color = updateCarRequest.getColor();
        this.description = updateCarRequest.getDescription();
        this.value = updateCarRequest.getValue();
    }
}
