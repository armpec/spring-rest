package com.example.springrest.repositories;

import com.example.springrest.entities.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CarRepository extends JpaRepository<Car, Long> {

    List<Car> findAllByColor(String color);

    @Query("FROM Car c WHERE UPPER(c.manufacturer) LIKE UPPER(CONCAT('%', :search, '%')) " +
            "OR UPPER(c.model) LIKE UPPER(CONCAT('%', :search, '%')) OR UPPER(c.description) LIKE UPPER(CONCAT('%', :search, '%'))")
    List<Car> searchCars(@Param("search") String search);

    @Query("FROM Car c WHERE c.color = :color AND (UPPER(c.manufacturer) LIKE UPPER(CONCAT('%', :search, '%')) " +
            "OR UPPER(c.model) LIKE UPPER(CONCAT('%', :search, '%')) OR UPPER(c.description) LIKE UPPER(CONCAT('%', :search, '%')))")
    List<Car> searchCars(@Param("search") String search, @Param("color") String color);

    List<Car> findAllByOwnerCompanyId(Long companyId);
}
