package com.example.springrest.controllers;

import com.example.springrest.dtos.CompanyDTO;
import com.example.springrest.services.CompanyService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static java.util.stream.Collectors.toList;

@RestController
@RequestMapping("/companies")
public class CompanyController {

    private final CompanyService companyService;

    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }

    @GetMapping
    public List<CompanyDTO> getCompanies() {
        return companyService.getCompanies().stream().map(CompanyDTO::new).collect(toList());
    }
}
