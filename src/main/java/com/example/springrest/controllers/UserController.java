package com.example.springrest.controllers;

import com.example.springrest.dtos.CarDTO;
import com.example.springrest.dtos.UserDTO;
import com.example.springrest.dtos.requests.AssignCarRequest;
import com.example.springrest.services.CarService;
import com.example.springrest.services.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

@RestController
@RequestMapping(value = "/users")
public class UserController {

    private final UserService userService;

    private final CarService carService;

    public UserController(UserService userService, CarService carService) {
        this.userService = userService;
        this.carService = carService;
    }

    @GetMapping
    @PreAuthorize("hasRole('ADMIN')")
    public List<UserDTO> getUsers() {
        return userService.getUsers().stream().map(UserDTO::new).collect(toList());
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN') || principal.id == #id")
    public UserDTO getUser(@PathVariable("id") Long id) {
        return new UserDTO(userService.getUser(id));
    }

    @GetMapping("/{id}/cars")
    @PreAuthorize("hasRole('ADMIN') || principal.id == #id")
    public Set<CarDTO> getUserCars(@PathVariable("id") Long id) {
        return userService.getUser(id).getCars().stream().map(CarDTO::new).collect(toSet());
    }

    @PatchMapping("/{id}/cars")
    @PreAuthorize("hasRole('ADMIN')")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void assignCar(@PathVariable("id") Long id, @Valid @RequestBody AssignCarRequest assignCarRequest) {
        carService.setOwner(assignCarRequest.getCarId(), id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasRole('ADMIN') || principal.id == #id")
    public void deleteCar(@PathVariable("id") Long id) {
        userService.deleteUser(id);
    }
}
