package com.example.springrest.controllers;

import com.example.springrest.dtos.CarDTO;
import com.example.springrest.dtos.requests.CreateCarRequest;
import com.example.springrest.dtos.requests.SetOwnerRequest;
import com.example.springrest.dtos.requests.UpdateCarRequest;
import com.example.springrest.dtos.responses.CreateCarResponse;
import com.example.springrest.dtos.responses.UpdateCarResponse;
import com.example.springrest.entities.Car;
import com.example.springrest.entities.User;
import com.example.springrest.services.CarService;
import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static java.util.stream.Collectors.toList;

@RestController
@RequestMapping(value = "/cars")
@Api(tags = "CarController")
public class CarController {

    private final CarService carService;

    public CarController(CarService carService) {
        this.carService = carService;
    }

    @ApiOperation(value = "Get all cars", tags = "getCars", httpMethod = "GET")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully get list of cars"),
            @ApiResponse(code = 401, message = "Unauthorized")
    })
    @GetMapping
    @PreAuthorize("hasRole('ADMIN')")
    public List<CarDTO> getCars(@ApiParam(value = "Car color", example = "black")
                                @RequestParam(required = false, value = "color") String color,
                                @ApiParam(value = "Car company", example = "1")
                                @RequestParam(required = false, value = "companyId") Long companyId,
                                @ApiParam(value = "Search by car fields: manufacturer, model, description", example = "toyota")
                                @RequestParam(required = false, value = "search") String search) {
        return carService.getCars(color, search, companyId).stream().map(CarDTO::new).collect(toList());
    }

    @ApiOperation(value = "Get car by id", tags = "getCar", httpMethod = "GET")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully get car record by id"),
            @ApiResponse(code = 404, message = "Car not found error"),
            @ApiResponse(code = 401, message = "Unauthorized")
    })
    @GetMapping("/{id}")
    public CarDTO getCar(@PathVariable("id") Long id) {
        return new CarDTO(carService.getCar(id));
    }

    @ApiOperation(value = "Create car", tags = "createCar", httpMethod = "POST")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully update car record"),
            @ApiResponse(code = 400, message = "Validation failed"),
            @ApiResponse(code = 401, message = "Unauthorized")
    })
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CreateCarResponse createCar(@Valid @RequestBody CreateCarRequest createCarRequest, @AuthenticationPrincipal User user) {
        return new CreateCarResponse(carService.createCar(
                new Car(createCarRequest), user.isAdmin() ? createCarRequest.getOwnerId() : user.getId()));
    }

    @ApiOperation(value = "Update car", tags = "updateCar", httpMethod = "PUT")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully create car record"),
            @ApiResponse(code = 400, message = "Validation failed"),
            @ApiResponse(code = 401, message = "Unauthorized")
    })
    @PutMapping("/{id}")
    public UpdateCarResponse updateCar(@PathVariable("id") Long id, @Valid @RequestBody UpdateCarRequest updateCarRequest,
                                       @AuthenticationPrincipal User user) {
        return new UpdateCarResponse(carService.updateCar(
                new Car(id, updateCarRequest), user.isAdmin() ? updateCarRequest.getOwnerId() : user.getId()));
    }

    @ApiOperation(value = "Set car owner", tags = "setOwner", httpMethod = "PATCH")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Successfully set car record owner"),
            @ApiResponse(code = 400, message = "Validation failed"),
            @ApiResponse(code = 401, message = "Unauthorized")
    })
    @PatchMapping("/{id}/owner")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void setOwner(@PathVariable("id") Long id, @Valid @RequestBody SetOwnerRequest setOwnerRequest) {
        carService.setOwner(id, setOwnerRequest.getOwnerId());
    }

    @ApiOperation(value = "Delete car by id", tags = "getCar", httpMethod = "DELETE")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Successfully delete car by id"),
            @ApiResponse(code = 404, message = "Car not found error"),
            @ApiResponse(code = 401, message = "Unauthorized")
    })
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCar(@PathVariable("id") Long id) {
        carService.deleteCar(id);
    }
}
