package com.example.springrest.controllers;

import com.example.springrest.dtos.UserDTO;
import com.example.springrest.dtos.requests.RegisterRequest;
import com.example.springrest.dtos.responses.LoginResponse;
import com.example.springrest.entities.User;
import com.example.springrest.services.JwtService;
import com.example.springrest.services.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class MainController {

    private final JwtService jwtService;

    private final UserService userService;

    public MainController(JwtService jwtService, UserService userService) {
        this.jwtService = jwtService;
        this.userService = userService;
    }

    @GetMapping
    public String home() {
        return "OK";
    }

    @GetMapping("/http")
    public ResponseEntity<Integer> http(@RequestParam("status") Integer status) {
        return new ResponseEntity<>(status, HttpStatus.valueOf(status));
    }

    @PostMapping("/login")
    public LoginResponse login(@AuthenticationPrincipal User user) {
        return new LoginResponse(jwtService.createToken(user), new UserDTO(user));
    }

    @PostMapping("/register")
    @ResponseStatus(HttpStatus.CREATED)
    public UserDTO register(@Valid @RequestBody RegisterRequest registerRequest) {
        return new UserDTO(userService.createUser(
                new User(registerRequest), registerRequest.getCompanyId(), registerRequest.getIsAdmin()));
    }
}
