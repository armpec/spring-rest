package com.example.springrest.repositories;

import com.example.springrest.entities.Car;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
class CarRepositoryTest {

    @Autowired
    private CarRepository carRepository;

    @Test
    public void testFindAllByColor() {
        // given
        // data from data.sql

        // when
        List<Car> cars = carRepository.findAllByColor("black");

        // then
        assertEquals(1, cars.size());
        assertEquals("black", cars.get(0).getColor());
    }

    @Test
    public void testSearchCarsByManufacturer() {
        // given
        // data from data.sql

        // when
        List<Car> cars = carRepository.searchCars("yO");

        // then
        assertEquals(1, cars.size());
        assertEquals(1L, cars.get(0).getId());
    }


    @Test
    public void testSearchCarsByModel() {
        // given
        // data from data.sql

        // when
        List<Car> cars = carRepository.searchCars("Iu");

        // then
        assertEquals(1, cars.size());
        assertEquals(1L, cars.get(0).getId());
    }
}