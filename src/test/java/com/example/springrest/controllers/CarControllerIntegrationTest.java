package com.example.springrest.controllers;

import com.example.springrest.dtos.requests.CreateCarRequest;
import com.example.springrest.dtos.responses.CreateCarResponse;
import com.example.springrest.dtos.responses.ErrorResponse;
import com.example.springrest.entities.Car;
import com.example.springrest.enums.Error;
import com.example.springrest.repositories.CarRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

@AutoConfigureMockMvc
@WithUserDetails("admin")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class CarControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void testGetCarWhenCarExists() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/cars/{id}", 2L))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

        Car car = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), Car.class);

        assertEquals(2L, car.getId());
    }

    @Test
    public void testGetCarWhenCarDoesNotExist() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/cars/{id}", 3L))
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andReturn();

        ErrorResponse errorResponse = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), ErrorResponse.class);

        assertEquals(404, errorResponse.getStatus());
        assertEquals(Error.RESOURCE_NOT_FOUND, errorResponse.getCode());
        assertEquals("Resource with id = 3 was not found", errorResponse.getMessage());
    }

    @Test
    public void testCreateCar() throws Exception {
        LocalDateTime now = LocalDateTime.now();

        CreateCarRequest createCarRequest = new CreateCarRequest(
                1L,
                "Test",
                "Test",
                1999,
                "green",
                "...",
                BigDecimal.valueOf(1999.99)
        );

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/cars")
                .content(objectMapper.writeValueAsString(createCarRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andReturn();

        CreateCarResponse createCarResponse = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), CreateCarResponse.class);

        // ar irasas tikrai yra db???
        Car car = carRepository.findById(createCarResponse.getId()).get();

        // ar irasas yra toks, kokio norejom??
        assertEquals(createCarRequest.getManufacturer(), car.getManufacturer());
        assertEquals(createCarRequest.getModel(), car.getModel());
        assertEquals(createCarRequest.getYear(), car.getYear());
        assertEquals(createCarRequest.getColor(), car.getColor());
        assertEquals(createCarRequest.getDescription(), car.getDescription());
        assertEquals(createCarRequest.getValue(), car.getValue());

        // ar created, updated ok?
        assertNotNull(car.getCreated());
        assertNotNull(car.getUpdated());
        assertTrue(car.getCreated().isAfter(now));
        assertTrue(car.getUpdated().isAfter(now));

        // ar response'as klientui atitinka įrašą?
        assertEquals(car.getManufacturer(), createCarResponse.getManufacturer());
        assertEquals(car.getModel(), createCarResponse.getModel());
        assertEquals(car.getYear(), createCarResponse.getYear());
        assertEquals(car.getColor(), createCarResponse.getColor());
        assertEquals(car.getDescription(), createCarResponse.getDescription());
        assertEquals(car.getValue(), createCarResponse.getValue());
    }

    @Test
    public void testCreateCarWithInvalidBody() throws Exception {
        CreateCarRequest createCarRequest = new CreateCarRequest(
                1L,
                "",
                "Test",
                1999,
                "green",
                "...",
                BigDecimal.valueOf(1999.99)
        );

        int size = carRepository.findAll().size();

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/cars")
                .content(objectMapper.writeValueAsString(createCarRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();
        ErrorResponse errorResponse = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), ErrorResponse.class);

        assertEquals(400, errorResponse.getStatus());
        assertEquals(Error.VALIDATION_FAILED, errorResponse.getCode());
        assertEquals("Invalid request body provided", errorResponse.getMessage());
        assertEquals(size, carRepository.findAll().size());
    }

    @Test
    public void testDeleteCarWhenCarExists() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/cars/{id}", 1L))
                .andExpect(MockMvcResultMatchers.status().isNoContent());

        assertFalse(carRepository.existsById(1L));
    }
}