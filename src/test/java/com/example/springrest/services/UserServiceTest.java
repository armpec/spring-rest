package com.example.springrest.services;

import com.example.springrest.entities.Company;
import com.example.springrest.entities.Role;
import com.example.springrest.entities.User;
import com.example.springrest.exceptions.UserAlreadyExistsException;
import com.example.springrest.repositories.RoleRepository;
import com.example.springrest.repositories.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    @Mock
    private CompanyService companyService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private RoleRepository roleRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private UserService userService;

    @Test
    public void testCreateUserWhenUsernameDoesNotExist() {
        Company company = new Company();
        company.setId(1L);

        User user = new User();
        user.setUsername("test123");
        user.setPassword("123456");
        user.setCompany(company);

        Role role = new Role();
        role.setName("USER");

        when(userRepository.findUserByUsername(user.getUsername())).thenReturn(Optional.empty());
        when(roleRepository.findRoleByName("USER")).thenReturn(Optional.of(role));
        when(passwordEncoder.encode(user.getPassword())).thenReturn("encoded");
        when(companyService.getCompany(1L)).thenReturn(company);
        when(userRepository.save(user)).thenReturn(user);

        userService.createUser(user, 1L, false);

        verify(userRepository, times(1)).save(user);
        verify(roleRepository, times(1)).findRoleByName("USER");
        verify(passwordEncoder, times(1)).encode("123456");

        assertEquals(1, user.getRoles().size());
        assertTrue(user.getRoles().contains(role));
        assertEquals("encoded", user.getPassword());
    }

    @Test
    public void testCreateUserWhenUsernameExists() {
        User user = new User();
        user.setUsername("test123");

        when(userRepository.findUserByUsername(user.getUsername())).thenReturn(Optional.of(user));

        assertThrows(UserAlreadyExistsException.class, () -> userService.createUser(user, 1L, false));

        verify(userRepository, never()).save(any(User.class));
    }
}
