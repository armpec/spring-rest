package com.example.springrest.services;

import com.example.springrest.entities.Car;
import com.example.springrest.entities.User;
import com.example.springrest.exceptions.ResourceNotFoundException;
import com.example.springrest.repositories.CarRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CarServiceTest {

    @Mock
    private CarRepository carRepository;

    @Mock
    private UserService userService;

    @Mock
    private Car car;

    @Mock
    private User user;

    @Mock
    private Authentication authentication;

    @Mock
    private SecurityContext securityContext;

    @InjectMocks
    private CarService carService;

    @Test
    void testGetCarWhenCarExists() {
        SecurityContextHolder.setContext(securityContext);

        when(securityContext.getAuthentication()).thenReturn(authentication);
        when(SecurityContextHolder.getContext().getAuthentication().getPrincipal()).thenReturn(user);
        when(car.getOwner()).thenReturn(user);
        when(carRepository.findById(1L)).thenReturn(Optional.of(car));

        Car response = carService.getCar(1L);

        assertEquals(car, response);
    }

    @Test
    void testGetCarWhenCarDoesNotExist() {
        SecurityContextHolder.setContext(securityContext);

        when(securityContext.getAuthentication()).thenReturn(authentication);
        when(SecurityContextHolder.getContext().getAuthentication().getPrincipal()).thenReturn(user);
        when(carRepository.findById(1L)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> carService.getCar(1L));
    }

    @Test
    public void testCreateCar() {
        when(userService.getUser(user.getId())).thenReturn(user);
        when(carRepository.save(car)).thenReturn(car);

        carService.createCar(car, user.getId());

        verify(carRepository, times(1)).save(car);
    }

    @Test
    public void testGetCarsByColor() {
        when(carRepository.findAllByColor("black")).thenReturn(List.of(car));

        List<Car> cars = carService.getCars("black", null, null);

        verify(carRepository, times(1)).findAllByColor("black");
        assertFalse(cars.isEmpty());
    }
}